# mithril-starter-kit

> Quick start for [mithril](https://mithril.js.org) with jsx, es6, webpack 4, stylus, hot reload and eslint (standard)

Basically, it's [mithril-starter-kit](https://github.com/dhinesh03/mithril-starter-kit) + [vuejs-templates webpack](https://github.com/vuejs-templates/webpack)

## How to use

```bash
yarn global add vue-cli
vue init up9cloud/mithril-starter-kit my-project
cd my-project
yarn dev
```
